const AdminBro = require('admin-bro')
const AdminBroExpress = require('@admin-bro/express')
const AdminBroMongoose = require('@admin-bro/mongoose')
// const AdminBroExpress = require('admin-bro-expressjs')
const express = require('express')
const app = express()
const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const adminBro = new AdminBro()

const auth = async (email, password) => {
    if (email === 'admin@example.com' && password === 'password') {
        return { email: 'admin@example.com', password: 'password' }
    }
    return null
}

AdminBro.registerAdapter(AdminBroMongoose)

const run = async () => {
    const connection = await mongoose.connect("mongodb://127.0.0.1:27017/admin", {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    console.log("connected with db")

    const Userschema = new mongoose.Schema({
        name: String,
        email: String,
    });



    const User = mongoose.model('User', Userschema)
    const Employee = mongoose.model('Employee', { id: String, name: String, email: String, surname: String, skills: [{ name: String }], birtdate: Date, comments: [{ Description: String, adult: Boolean }], contact: Number })
    const Adminuserschema = new Schema({
        role: String,
        contact:Number
    });
    const AdminUser = mongoose.model('AdminUser', Adminuserschema)
    const adminBro = new AdminBro({
        Databases: [connection],
        rootPath: '/admin',
        resources: [AdminUser, User,Employee]
    })
    const router = AdminBroExpress.buildAuthenticatedRouter(adminBro, {
        authenticate: auth,
        cookieName: 'adminbro',
        cookiePassword: 'some-secret-password',
    })
    app.use('/admin', router)
    app.listen(4000, () => console.log('Server started on http://localhost:4000'))


}
run()



